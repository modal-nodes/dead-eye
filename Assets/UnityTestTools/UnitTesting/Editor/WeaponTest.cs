﻿using NUnit.Framework;
using UnityEngine;

[TestFixture]
public class WeaponTest {

	//Checks to see if health is updated after taking damage
	[Test]
	public void FireWeapon_DamageCheck() 
	{
		var weapon = new Weapon (6, 5, 100, 1, "TestWeapon");
		int damage = weapon.calculateDamage (10);
		Assert.AreEqual (90, damage);

	}
		
}