﻿using NUnit.Framework;
using System.Collections;
using UnityEngine;

[TestFixture]
public class HealthTest {

	//Checks to see if health is updated after taking damage
	[Test]
	public void TakeDamage_HealthUpdated() 
	{
		var health = new HealthManager ();
		health.setCurrentHealth (50);
		health.damagePlayer (10);

		Assert.AreEqual(40, health.getCurrentHealth());
	}


}