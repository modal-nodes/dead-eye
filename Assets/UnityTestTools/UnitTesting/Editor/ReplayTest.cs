﻿using NUnit.Framework;
using NSubstitute;
using System.Collections;
using UnityEngine;

[TestFixture]
public class ReplayTest {

	//Checks to see if health is updated after taking damage
	[Test]
	public void SaveFrame_CheckConversion()
	{
		//First set to values
		GameObject go = new GameObject();
		PlayerReplay replay = go.AddComponent<PlayerReplay>();

		replay.ic.rotateX = 1;
		replay.ic.rotateY = 1;
		replay.ic.jump = true;
		replay.ic.shoot = false;
		replay.ic.health = 100;

		//Then write to stream
		replay.SaveState ();

		//Then overwrite values
		replay.ic.speed = new Vector3 (0, 0, 1);
		replay.ic.rotateX = 2;
		replay.ic.rotateY = 2;
		replay.ic.jump = false;
		replay.ic.shoot = true;
		replay.ic.health = 10;

		//Now we load from stream and see if it converted back
		replay.LoadFrame();

		Assert.AreEqual (new Vector3 (0, 0, 0), replay.ic.speed);
		Assert.AreEqual (1, replay.ic.rotateX);
		Assert.AreEqual (1, replay.ic.rotateY);
		Assert.AreEqual (true, replay.ic.jump);
		Assert.AreEqual (false, replay.ic.shoot);
		Assert.AreEqual (100, replay.ic.health);

	
	}
		


}
	