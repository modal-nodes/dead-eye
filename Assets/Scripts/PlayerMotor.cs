using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerMotor : NetworkBehaviour
{
	private Camera cam;
	private Vector3 camOffset = new Vector3(0f,1.00f,0.17f);
	private float jumpSpeed = 4f;
	private CharacterController cc;

	private float verticalRotation = 0;
	private float horizontalRotation = 0;

	private Vector3 moveVector;

	private float verticalRotateSpeed = 0.2f;
	private float horizontalRotateSpeed = 0.6f;

	private float verticalVelocity;
	private float upDownRange = 60.0f;

	private bool isAlive = true;
	//Variable to change how fast the camera zooms out when the player dies
	private float camZoomChange = 0.02f;

	// The amount the camera snaps back after death
	private float camSnapAmount = 2;

	// Variable if the camera has yet moved back to see the action
	private bool hasCamMoved = false;

	void Start ()
	{
		if (!isLocalPlayer) {
			return;
		}
		cc = GetComponent<CharacterController> ();
		cam = Camera.main;

		this.verticalVelocity = 0;
	}

	// Called by health manager
	public void setisAlive(bool newIsAlive) {
		isAlive = newIsAlive;
		hasCamMoved = false;

	}

	// Update is called once per frame

	void Update () {

		if (!isLocalPlayer) {
			return;
		}

		if (!cc.isGrounded) {
			verticalVelocity += Physics.gravity.y * Time.deltaTime;
		}
		//Move with stored movement vector (from player controller) and gravitational vertical velocity
		movePlayer(new Vector3(moveVector.x,verticalVelocity,moveVector.z));

		// Checks if the player is in a valid position
		checkValidPosition ();

		// Get camera to follow player
		if (isAlive) {
			cam.transform.position = this.transform.position + camOffset;
		} else {
			// Snap the camera back an amount, to see the death
			if (!hasCamMoved) {
				hasCamMoved = true;
				cam.transform.position  = cam.transform.position  - new Vector3(0,0,camSnapAmount);
			}
			// Pan out camera to see dying player
			cam.transform.position  = cam.transform.position  - new Vector3(0,0,camZoomChange);
		}
	}

	// Moves the player if they are in an invalid position, ie outside the fences
	void checkValidPosition () {
		float minX = -45;
		float maxX = 45;
		float minZ = -47;
		float maxZ = 47;
		Vector3 newPos = transform.position;
		newPos.x = Mathf.Clamp (newPos.x, minX, maxX);
		newPos.z = Mathf.Clamp (newPos.z, minZ, maxZ);
		transform.position = newPos;
	}

	// Use a single camera in the scene, but make sure it's following the correct player
	void OnNetworkInstantiate(NetworkMessageInfo message) {
		if (GetComponent<NetworkView>().isMine) {
			cam = Camera.main;
		}
	}

	public void rotatePlayer(float deltax, float deltay) {

		horizontalRotation -= deltax * horizontalRotateSpeed;
		verticalRotation -= deltay * verticalRotateSpeed;
		verticalRotation = Mathf.Clamp (verticalRotation, -upDownRange, upDownRange);
		transform.rotation = Quaternion.Euler (0,horizontalRotation, 0);
		// Rotate the camera
		cam.transform.rotation = Quaternion.Euler (verticalRotation,horizontalRotation, 0);
	}

	public void movePlayer(Vector3 movement) {
		if (isAlive) {
			
			movement = transform.rotation * movement;
			//Move the player
			cc.Move (movement * Time.deltaTime );
		}
	}

	public void setMoveVector(Vector3 moveVector) {
		this.moveVector = moveVector;
	}
	public void jump() {
		if (isAlive) {
			Debug.Log ("Jumping");
			if (cc.isGrounded) {
				Debug.Log ("Got past ground check");
				verticalVelocity = jumpSpeed;
			}
		}
	}

}
