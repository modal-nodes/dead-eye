﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.IO;
using System.Linq;

public class PlayerReplay : NetworkBehaviour {


	private MemoryStream stream;
	public InputCollection ic;

	private Vector3 startPosition;
	private Quaternion startRotation;
	private string startWeapon;

	private int bytesperframe = 24;

	void Start () {

		ic = new InputCollection ();
		stream = new MemoryStream ();
		//setStartConditions ();
	}

	//This method is used for saving the state of the player each frame in order to play it back later.
	//ALWAYS IN THE SAME ORDER!!! Very important!
	public void SaveState()
	{

		//Write movement vector to memory first
		stream.Write (BitConverter.GetBytes (ic.speed.x), 0, 4); //Use bitconverter to change from float to bytes
		stream.Write (BitConverter.GetBytes (ic.speed.y), 0, 4);
		stream.Write (BitConverter.GetBytes (ic.speed.z), 0, 4);

		//Then write rotate floats
		stream.Write (BitConverter.GetBytes (ic.rotateX), 0, 4);
		stream.Write (BitConverter.GetBytes (ic.rotateY), 0, 4);

		//Then write jump/shoot
		stream.Write(BitConverter.GetBytes (ic.jump), 0, 1);
		stream.Write(BitConverter.GetBytes (ic.shoot), 0, 1);

		stream.Write(BitConverter.GetBytes (ic.health), 0, 2);

	}

	public void LoadFrame() {
		byte[] thisInput = loadInputStream();

		InputCollection frameIC = new InputCollection ();

		//Convert the byte array into the InputCollection's datatypes
		frameIC.speed.x = BitConverter.ToSingle(thisInput, 0);
		//Debug.Log (frameIC.speed.x);
		frameIC.speed.y = BitConverter.ToSingle(thisInput, 4);
		//Debug.Log (frameIC.speed.y);
		frameIC.speed.z = BitConverter.ToSingle(thisInput, 8);
		//Debug.Log (frameIC.speed.z);

		frameIC.rotateX = BitConverter.ToSingle (thisInput, 12);
		//Debug.Log (frameIC.rotateX);
		frameIC.rotateY = BitConverter.ToSingle (thisInput, 16);
		//Debug.Log (frameIC.rotateY);

		frameIC.jump = BitConverter.ToBoolean (thisInput, 20);
		//Debug.Log (frameIC.jump);
		if (frameIC.jump) Debug.Log("Replayed jump");

		frameIC.shoot = BitConverter.ToBoolean (thisInput, 21);
		//Debug.Log (frameIC.shoot);
		if (frameIC.shoot) Debug.Log("Replayed shoot");

		frameIC.health = BitConverter.ToInt16 (thisInput, 22);
		//Debug.Log (frameIC.health);


		//Set this to be this object's InputCollection
		this.ic = frameIC;
	}

	private byte[] loadInputStream() {
		//Create new byte array for this frame's input
		byte[] thisInput = new byte[bytesperframe];

		//Copy the frame's bytes from the memorystream
		try {
			Buffer.BlockCopy (stream.ToArray(), 0, thisInput, 0, thisInput.Length);

			//Delete those bytes from the memory stream
			byte[] newArray = stream.ToArray().Skip(bytesperframe).ToArray();
			MemoryStream newStream = new MemoryStream(newArray);
			this.stream = newStream;
		} catch {
			Debug.Log ("Reached End");
		}
		return thisInput;

	}

	[Command]
	public void CmdClearStream() {
		RpcClearStream ();
	}

	[ClientRpc]
	private void RpcClearStream() {
		foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player")) {
			// This method has an is local player check :)
			PlayerReplay otherPlayer = player.GetComponent<PlayerReplay> ();
			otherPlayer.clearStream ();
		}
	}

	public void clearStream() {
		stream = new MemoryStream ();
		setStartConditions ();
	}

	private void setStartConditions() {
		startPosition = this.transform.position;
		startRotation = this.transform.rotation;
		PlayerWeapon pw = GetComponent<PlayerWeapon> ();
		try {
			startWeapon = pw.getCurrentWeapon ();
		} catch {
			startWeapon = "Pistol";
		}
	}

	public Vector3 getStartPosition() {
		return this.startPosition;
	}

	public Quaternion getStartRotation() {
		return this.startRotation;
	}

	public string getStartWeapon() {
		return this.startWeapon;
	}

	public class InputCollection {
		public Vector3 speed;
		public float rotateX;
		public float rotateY;
		public bool jump;
		public bool shoot;
		public int health;

		public void setSpeed(Vector3 speed) {
			this.speed = speed;
		}

		public void setRotateX(float rotateX) {
			this.rotateX = rotateX;
		}

		public void setRotateY(float rotateY) {
			this.rotateY = rotateY;
		}
	}
}
