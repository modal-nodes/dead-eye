using UnityEngine;
using System.Collections;

public class GameRules
{
	// Whether players on the same team can damage each other
	private bool friendlyFire = true;
	private int maxHealth = 100;
	// How much health is gained on each regeneration
	private int regenAmount = 10;
	// How long before a player starts regeneration
	private float timeThreshold = 1.0f;
	private int startingHealth = 100;
	// How long the player has to wait to respawn
	private float timeToRespawn = 5f;

	// This value is the time taken between regen steps
	private float timeThresholdForRegen = 0.5f;

	public bool getFriendlyFire()
	{
		return friendlyFire;
	}

	public int getMaxHealth()
	{
		return maxHealth;
	}
	public int getRegenAmount()
	{
		return regenAmount;
	}
	public float getTimeThreshold()
	{
		return timeThreshold;
	}

	public float getTimeThresholdForRegen()
	{
		return timeThresholdForRegen;
	}

	public int getStartingHealth() {
		return startingHealth;
	}
	public float getTimeToRespawn() {
		return timeToRespawn;
	}
}
