﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;


// This class controls the gameplay, from rules to spawning.

public class GameController : NetworkBehaviour {
	// Access points for other parts of the System
	// See the SD for interactions
	public SpawnController spawnController;
	public TeamIdent teamManager;
	public PlayerIdent playerManager;
	public Scoring scores;
	public GameRules gameRules;
	public bool isPlayback = false;

	// If the game is over, functionality is restricted
	public bool isGameOver = false;

	void Awake() {
		spawnController = new SpawnController();
		teamManager = GetComponent<TeamIdent> ();
		playerManager = GetComponent<PlayerIdent> ();
		scores = GetComponent<Scoring> ();
		gameRules = new GameRules ();
	}

	// Damages a single player, and possibly respawns and scores them
	public void selfHalm(GameObject player, int damage) {
		HealthManager playerHealthScript = player.GetComponent<HealthManager>();
		int oldHealth = playerHealthScript.getCurrentHealth ();

		if (isServer) {
			// Can just change it's local copy.
			playerHealthScript.damagePlayer (damage);
		} else {
			// Not the server. But cannot call a command on another client.
			// Lets call a local command, which will then call damage player
			PlayerCommands shootersScript = player.GetComponent<PlayerCommands>();
			shootersScript.CmdgiveDamage (player, damage);
		}
		// Need to be calculated here as the clients SyncVar may not have updated yet
		int newHealth = oldHealth - damage;

		if (newHealth <= 0) {
			// Respawn the player!
			Vector3 newSpawnLoc = spawnController.getNextSpawnLocation ();

			if (isServer) {
				// Show the death on the chat
				reportDeath (player);
				//displayScores ();
				scores.scoreDeath (player, player);
				playerHealthScript.RpcRespawn (newSpawnLoc);
			} else {
				// Have to go through a script attached to the player in order
				// To get command authority
				PlayerCommands shootersScript = player.GetComponent<PlayerCommands>();
				shootersScript.CmdRespawnAnotherPlayer (player,newSpawnLoc);
			}
		}
	}


	public void damageThePlayer(GameObject playerWhoShot, GameObject playerWhoWasHit, int damage) {
		HealthManager playerHealthScript = playerWhoWasHit.GetComponent<HealthManager>();
		int oldHealth = playerHealthScript.getCurrentHealth ();

		if (isServer) {
			// Can just change it's local copy.
			playerHealthScript.damagePlayer (damage);
		} else {
			// Not the server. But cannot call a command on another client.
			// Lets call a local command, which will then call damage player
			PlayerCommands shootersScript = playerWhoShot.GetComponent<PlayerCommands>();
			shootersScript.CmdgiveDamage (playerWhoWasHit, damage);
		}

		int newHealth = oldHealth - damage;

		if (newHealth <= 0) {
			// Respawn the player!
			Vector3 newSpawnLoc = spawnController.getNextSpawnLocation ();

			if (isServer) {
				reportDeath (playerWhoWasHit);
				// Respawn the player on all clients
				playerHealthScript.RpcRespawn (newSpawnLoc);
				// Score the death, need a RPC function
				// So it is scored on each client
				// But the server still has the "real" copy
				RpcScorePlayers(playerWhoShot, playerWhoWasHit);

			} else {
				// Need to go through playerCommand for Command authority
				PlayerCommands shootersScript = playerWhoShot.GetComponent<PlayerCommands>();
				shootersScript.CmdRespawnAnotherPlayer (playerWhoWasHit,newSpawnLoc);
				shootersScript.CmdScorePlayers (playerWhoShot, playerWhoWasHit);
			}
		}
	}

	// This method is called by PlayerWeapon when a hit happend on a player object
	// This method decides if damage needs to be dealt
	public void hit (GameObject playerWhoShot, GameObject playerWhoWasHit, int damage) {
	  int teamOfShooter   = playerWhoShot.GetComponent<PlayerTeam>().getTeam();
		int teamOfWhoWasHit = playerWhoWasHit.GetComponent<PlayerTeam>().getTeam();

		if (teamOfShooter == teamOfWhoWasHit) {
			if (gameRules.getFriendlyFire()) {
				// Need to pass in both players as scoring happens later
				damageThePlayer (playerWhoShot, playerWhoWasHit, damage);
			}
		} else {
			// Need to pass in both players as scoring happens later
			damageThePlayer (playerWhoShot, playerWhoWasHit, damage);
		}
	}

	// This is only called on the server, to alert all players to the sad news that is a players death
	private void reportDeath (GameObject playerWhoDied) {
		if (!isServer) {
			return;
		}
		// Find the players ID
		PlayerTeam pt = playerWhoDied.GetComponent<PlayerTeam>();
		int playerId = pt.getPlayerNum ();

		string message = "Player " + playerId.ToString () + " is dead!";

		// Report the news
		GameObject nwmg = (GameObject)GameObject.Find ("Network Manager");
		ImplementedNetworkManager inm = nwmg.GetComponent<ImplementedNetworkManager> ();
		inm.currentMessage = message;
		inm.sendMessage ();
	}

	// This is called by the localPlayer when they first join the game
	// This registers them, giving them a team and player id
	// And setting intial health
	public void addPlayer(GameObject player) {
		// Set players initial health
		int maxHealth = gameRules.getMaxHealth ();
		int startingHealth = gameRules.getStartingHealth ();

		HealthManager hm = player.GetComponent<HealthManager> ();
		hm.setMaxHealth (maxHealth);
		// Needs to be a Command, so the value is changed on the server
		// As it's a SyncVar
		hm.CmdsetCurrentHealth (startingHealth);

		// The amount gained on each health regen
		int regenAmount = gameRules.getRegenAmount ();
		hm.setHealthRegenAmount (regenAmount);

		// The time taken before regen starts
		float timeThreshold = gameRules.getTimeThreshold ();
		hm.setTimeThreshold (timeThreshold);

		// The time between regens
		float timeThresholdForRegen = gameRules.getTimeThresholdForRegen ();
		hm.setTimeThresholdForRegen (timeThresholdForRegen);

		// Needs to be a command, as the values are SyncVar
		// So must be correct on the server
		PlayerCommands pc = player.GetComponent<PlayerCommands> ();
		pc.CmdsetPlayerTeamAndIdent (player);

		// A new player has joined, we need to clear the replay stream
		// So all players will start the replay at the same time
		PlayerReplay pr = player.GetComponent<PlayerReplay> ();
		pr.CmdClearStream ();
	}

	// This is called using data from the server to all clients
	// So that all clients get the same score
	[ClientRpc]
	public void RpcScorePlayers(GameObject playerWhoKilled, GameObject playerWhoDied) {
		// Call score death
		scores.scoreDeath(playerWhoKilled, playerWhoDied);
	}

	// End the game for all players
	public void endGame() {
		RpcEndGame ();
	}

	// Called on each client, telling them the game is over
	[ClientRpc]
	public void RpcEndGame() {
		isGameOver = true;
		// Start the replay for everyone!
		foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player")) {
			// This method has an is local player check
			PlayerController pc = player.GetComponent<PlayerController> ();
			pc.gameOver ();

		}
	}

	// Called when the game is over
	// It disables the contorls
	// Waits 5 seconds, then starts the replay
	public void gameOver () {
		if (!isLocalPlayer) {
			return;
		}
		Debug.Log ("GAME OVER");
		isGameOver = true;

		// Stop movement of the player
		PlayerMotor pm = GetComponent<PlayerMotor> ();
		//pm.setIsGameOver (true);
		Debug.Log ("Starting timer");
		StartCoroutine(    WaitSomeTime (5f));
	}

	// Waits some time, then sets isPlayback to true
	IEnumerator WaitSomeTime(float seconds) {
		yield return new WaitForSeconds(seconds);
		Debug.Log ("Finished timer");

		// This starts the playback after the time has finished
		isPlayback = true;
	}
}
