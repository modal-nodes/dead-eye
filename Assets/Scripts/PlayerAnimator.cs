﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;


public class PlayerAnimator : NetworkBehaviour {

    // Animator object 
    Animator anim;
    // Define run state hash used in AnimatorController FSM 
    int runStateHash = Animator.StringToHash("Base Layer.Run");

    // Get the animator component 
    void Start ()
    {
        if (!isLocalPlayer)
            return;
        
        anim = GetComponent<Animator>();

    }

    // Update establishes the two axes of input and 
    // sends information to the animator accordingly. 
    // if statement used as a workaround to avoid constant 
    // movement when changing from Mono to NetworkBehaviour 
    void Update ()
    {
        if (!isLocalPlayer)
            return;

        float strafe = Input.GetAxis ("Horizontal");
        float move = Input.GetAxis ("Vertical");

        anim.SetFloat("Forward", move);
        
        if (strafe > 0.001) {
            anim.SetFloat("Strafe", strafe);
        }
        else if (strafe < 0.001) {
            anim.SetFloat("Strafe", strafe); 
        }
        
        AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);

    }

    public void Strafe() {
        if ((Input.GetAxis ("Strafe")) > 0) {
            GetComponent<Animation>().CrossFade ("strafeRight");
        } else if ((Input.GetAxis ("Strafe")) < 0) {
            GetComponent<Animation>().CrossFade ("strafeLeft");
        }
    }

}