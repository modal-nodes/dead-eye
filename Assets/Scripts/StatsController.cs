﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

//Schema for storing stats in playerprefs:
/**
	 * lastGameID - ID of last game
	 * Total number of players: G_Players
	 * Total number of teams: G_Teams
	 * Player X Kills in game G: G_X_PlayerKills $$
	 * Player X Deaths in game G: G_X_PlayerDeaths $$
	 * Player X's Team: G_X_Team
	 * Team X's Team kills in game G: G_X_TeamKills $$
	 * Team X's Team deaths in game G: G_X_TeamDeaths $$
	 * Total Kills in game G: G_Kills $$
	 * Total Deaths in game G: G_Deaths $$
	 * */


//Controller for handling presentation of the stats in the stats scene.

//Essentially MVC with the following interaction:
//Model - Playerprefs schema above
//View - Scene itself.
public class StatsController : MonoBehaviour {

	//Static class vars
	private static int w = Screen.width;
	private static int h = Screen.height;
	//Headers for the table
	private static readonly List<string> headers = new List<string>(new string[] { 
		"Game Number", 
		"P1 K/D/\nKDR",
		"P2 K/D/\nKDR",
		"P3 K/D/\nKDR",
		"P4 K/D/\nKDR",
		"Total Kills",
		"Total Deaths"});
	//styles
	private static GUIStyle tableStyle, headerStyle, buttonStyle;


	// Use this for initialization
	void Start () {
		//This should lock the screen to landscape on Android.
		Screen.autorotateToLandscapeLeft = true;
		Screen.autorotateToLandscapeRight = true;
		Screen.autorotateToPortrait = false;
		Screen.autorotateToPortraitUpsideDown = false;
		Screen.orientation = ScreenOrientation.AutoRotation;
	}

	//Handles all the GUI rendering.
	void OnGUI() {
		//Styling
		tableStyle = new GUIStyle (GUI.skin.GetStyle("box"));
		tableStyle.fontSize = 32;

		headerStyle = new GUIStyle (GUI.skin.GetStyle ("box"));
		headerStyle.fontSize = 32;
		headerStyle.wordWrap = true;

		buttonStyle = new GUIStyle (GUI.skin.GetStyle("button"));
		buttonStyle.fontSize = 40;
		buttonStyle.alignment = TextAnchor.MiddleCenter;

		//Let's draw the scores table:
		//Headers:
		for (int i = 0; i < headers.Count; i++) 
			GUI.Box(new Rect(4*w/15+i*w/15,3*h/10 , w/15, h/10), headers[i], headerStyle);

		//Reset stats button.
		if (GUI.Button (new Rect (0, 0, w / 6, h / 6), "Reset Stats", buttonStyle))
			PlayerPrefs.DeleteAll();

		//Back to Main Menu button.
		if (GUI.Button (new Rect (w / 6, h/10, w / 6, h / 10), "Back", buttonStyle))
			SceneManager.LoadScene ("MainMenu");
			

		//Early exit in case there are no games to show. 
		if (!PlayerPrefs.HasKey("lastGameID")) return;

		//Data
		//Get the number of games:
		int num_games = PlayerPrefs.GetInt ("lastGameID");
		//Gracefully set the guard in case there are less than 5 games.
		int start = num_games < 4 ? 0 : num_games - 4;

		//It took a bit of fiddling to get the table to display nicely. It's just ratios and a bit of intuition. 
		for (int i=start,k=0; i <= num_games; i++,k++) {
			//render the game number:
			GUI.Box(new Rect(4*w/15, (3+k+1)*h/10, w/15, h/10), (i+1).ToString(), tableStyle);
			//Generate and render the individual player stats
			//handle 4 players for the time being.
			for (int j = 1; j <= 4; j++) {
				int kills = PlayerPrefs.GetInt(i.ToString()+"_"+j.ToString()+"_PlayerKills");
				int deaths=PlayerPrefs.GetInt(i.ToString()+"_"+j.ToString()+"_PlayerDeaths");
				string kdr = deaths==0 ? "inf" : (kills * 1.0 / deaths).ToString();
				string stat_string = kills.ToString () + "/" + deaths.ToString () + "/" + kdr;
				GUI.Box(new Rect(4*w/15+(j)*w/15, (3+k+1)*h/10, w/15, h/10), stat_string, tableStyle);
			}
			//render the total kills and deaths
			GUI.Box(new Rect(4*w/15+5*w/15, (3+k+1)*h/10, w/15, h/10), PlayerPrefs.GetInt(i.ToString()+"_Kills").ToString(), tableStyle);
			GUI.Box(new Rect(4*w/15+6*w/15, (3+k+1)*h/10, w/15, h/10), PlayerPrefs.GetInt(i.ToString()+"_Deaths").ToString(), tableStyle);
		}
	}
}
