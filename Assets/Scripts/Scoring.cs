﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;


//Class to handle the scoring of kills/deaths
//Uses Playerprefs to store scores. (See Unity Documentation for more info.)
public class Scoring : NetworkBehaviour
{
	//Schema for storing in playerprefs:

	/**
	 * lastGameID - ID of last game
	 * Total number of players: G_Players
	 * Total number of teams: G_Teams
	 * Player X on Team Kills in game G: G_X_PlayerKills $$
	 * Player X on Team Deaths in game G: G_X_PlayerDeaths $$
	 * Player X's Team: G_X_Team
	 * Team X's Team kills in game G: G_X_TeamKills $$
	 * Team X's Team deaths in game G: G_X_TeamDeaths $$
	 * Total Kills in game G: G_Kills $$
	 * Total Deaths in game G: G_Deaths $$
	 * */

	//Instance variables 
	//These can probably be private but I don't want to break anything in case they're accessed externally. 
	int [] playerKills;
	int [] playerDeaths;
	int total_kills = 0;
	int total_deaths=0;
	int [] teamKills;
	int [] teamDeaths;
	int gameID;
	int maxNumKills = 8;

	// Use this for initialization
	void Start ()
	{	
			Debug.Log ("Starting the scoring stuff:");
			//get/set the lastgameID
			if (PlayerPrefs.HasKey ("lastGameID")) {
				gameID = PlayerPrefs.GetInt ("lastGameID") + 1;
				//increment the local storage
				PlayerPrefs.SetInt ("lastGameID", gameID);
			} else {
				gameID = 0;
				PlayerPrefs.SetInt ("lastGameID", gameID);
			}

			//Initialise all the instance variables
			playerKills = new int[4];
			playerDeaths = new int[4];
			teamKills = new int[4];
			teamDeaths = new int[4];
			for (int i = 1; i <= 4; i++) {
				playerKills [i-1] = 0;
				playerDeaths [i-1] = 0;
				teamKills [i-1] = 0;
				teamDeaths [i-1] = 0;
				//Initialise all the player prefs data to 0:
				PlayerPrefs.SetInt (gameID.ToString () + "_" + i.ToString () + "_PlayerKills", 0);
				PlayerPrefs.SetInt (gameID.ToString () + "_" + i.ToString () + "_PlayerDeaths", 0);
				//This will work for 4 teams:
				PlayerPrefs.SetInt (gameID.ToString () + "_" + i.ToString () + "_TeamKills", 0);
				PlayerPrefs.SetInt (gameID.ToString () + "_" + i.ToString () + "_TeamDeaths", 0);
			}
			//Initialise number of players and teams to 4:
			PlayerPrefs.SetInt (gameID.ToString () + "_Players", 4);
			PlayerPrefs.SetInt (gameID.ToString () + "_Teams", 4);
			//Initialise total kills/deaths to 0:
			PlayerPrefs.SetInt (gameID.ToString () + "_Kills", 0);
			PlayerPrefs.SetInt (gameID.ToString () + "_Deaths", 0);
	}


	//Scores a Kill/Death for the given two players. 
	public void scoreDeath(GameObject playerWhoShot, GameObject playerWhoDied) {

		Debug.Log ("Scoring a death! Woo! death!");
		PlayerTeam pcWhoShot = playerWhoShot.GetComponent<PlayerTeam> ();
		PlayerTeam pcWhoDied = playerWhoDied.GetComponent<PlayerTeam> ();

		int playerIDWhoShot = pcWhoShot.getPlayerNum ();
		int playerIDWhoDied = pcWhoDied.getPlayerNum ();

		int teamIDWhoShot = pcWhoShot.getTeam ();
		int teamIDWhodied = pcWhoDied.getTeam ();
		Debug.Log("Player " + playerIDWhoShot.ToString () + " just killed player " + playerIDWhoDied.ToString ());
		//Add the player/team kills/deaths using the below helper functions
		addPlayerDeath (playerIDWhoDied);
		addTeamDeath (teamIDWhodied);

		addPlayerKill (playerIDWhoShot);
		addTeamKill (teamIDWhoShot);
		//incrememnt the kill/death counters
		total_kills++;
		total_deaths++;
		//Increments the stored total kills/deaths
		PlayerPrefs.SetInt (gameID.ToString()+"_Kills", total_kills);
		PlayerPrefs.SetInt (gameID.ToString()+"_Deaths", total_deaths);
		//Handle displaying who killed who across the chat server, as well as ending the game.
		if (isServer) {
			string message = "Player " + playerIDWhoShot.ToString () + " just killed player " + playerIDWhoDied.ToString ();
			Debug.Log (message);
			// Show this kill in the chat box.
			GameObject nwmg = (GameObject)GameObject.Find ("Network Manager");
			ImplementedNetworkManager inm = nwmg.GetComponent<ImplementedNetworkManager> ();
			inm.currentMessage = message;
			inm.sendMessage ();

			//handle gameover stuff:
			if (total_kills > maxNumKills) {
				GameController gc = GetComponent<GameController> ();
				gc.endGame ();
			}
		}
	}


	//Adds a player death to the instance vars and the score storage.
	private void addPlayerDeath( int playerID) {
		int index = playerID;
		Debug.Log ("Player deaths");
		Debug.Log (index.ToString());
		Debug.Log (playerDeaths.Length);
		playerDeaths [index-1] += 1;
		PlayerPrefs.SetInt (gameID.ToString() + "_" + (playerID).ToString() + "_PlayerDeaths", playerDeaths [index-1]);
	}

	//Adds a player kill to the instance vars and the score storage.
	private void addPlayerKill(int playerID) {
		int index = playerID;
		Debug.Log ("Player kills");
		Debug.Log (index.ToString());
		Debug.Log (playerDeaths.Length);
		playerKills [index-1] += 1;
		//Debug.Log("Setting:" + gameID.ToString () + "_" + (playerID).ToString() + "_PlayerKills" +"to" + playerKills[index] 
		PlayerPrefs.SetInt (gameID.ToString () + "_" + (playerID).ToString() + "_PlayerKills", playerKills[index-1]);
	}

	//Adds a team death to the instance vars and the score storage.
	private void addTeamDeath(int teamID) {
		
		int index = teamID;
		Debug.Log ("Team Deaths");
		Debug.Log (index.ToString());
		Debug.Log (playerDeaths.Length);
		teamDeaths [index] += 1;
		PlayerPrefs.SetInt (gameID.ToString () + "_" + teamID.ToString()+ "_TeamDeaths", teamDeaths[index]);
	}

	//Adds a team kill to the instance vars and the score storage.
	private void addTeamKill(int teamID) {
		int index = teamID;
		Debug.Log ("Team kills");
		Debug.Log (index.ToString());
		Debug.Log (playerDeaths.Length);
		teamKills [index] += 1;
		PlayerPrefs.SetInt (gameID.ToString () + "_" + teamID.ToString() + "_TeamKills", teamDeaths [index]);
	}


	//Public getters for the player/team kills/deaths.
	
	public int getPlayerKills (int playerID) {
		int index = playerID - 1;
		return playerKills [index];
	}
	public int getPlayerDeaths (int playerID) {
		int index = playerID - 1;
		return playerDeaths [index];
	}

	public int getTeamKills (int teamID) {
		int index = teamID - 1;
		return teamKills [index];
	}
	public int getTeamDeaths (int teamID) {
		int index = teamID - 1;
		return teamDeaths [index];
	}

}

