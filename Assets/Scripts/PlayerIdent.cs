﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

// Simple class to get player Number for a new player
// To be used for scoring etc.
// The number will be saved in the player

public class PlayerIdent : NetworkBehaviour
{
	List<int> listOfPlayerIds;
	// Use this for initialization
	void Start ()
	{
		listOfPlayerIds = new List<int>();
		for (int i = 1; i <=4 ; i++) // Adding 1 to 4 inclusive
		{
			listOfPlayerIds.Add (i);
		}
	}

	// Get a new player id
	public int getNewId() {
		int newID = -1;
		if (listOfPlayerIds.Count > 0) {
			newID = listOfPlayerIds[0];
			listOfPlayerIds.RemoveAt(0);
		}
		return newID;
	}

	// A player left the game, so return the ID
	public void playerLeft(int oldID){
		listOfPlayerIds.Add (oldID);
		listOfPlayerIds.Sort (); // Sorts the list in case of misordering
	}

}
