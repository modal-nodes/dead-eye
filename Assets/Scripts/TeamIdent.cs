﻿﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

// Class to identify a player's team 
public class TeamIdent : MonoBehaviour
{

	private int numPlayersAdded = 0;
	private int maxPlayers = 4;
	private int numTeams = 2;
	private int numOnTeam1 = 0 ;
	private int numOnTeam2 = 0;

	// Get team number for a new player
	// return -1 if no teams available/game full 
	public int getTeam() {
		if (numPlayersAdded >= maxPlayers) {
			return -1;
		}
		else if (numOnTeam1 <= numOnTeam2) {
			numOnTeam1 += 1;
			numPlayersAdded += 1;
			return 1;
		}
		else {
			numOnTeam2 += 1;
			numPlayersAdded += 1;
			return 2;
		}
	}
}
