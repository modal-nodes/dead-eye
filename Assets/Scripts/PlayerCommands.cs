﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

// This class is used when the GameContoller needs Command authority
// All of these methods can be thought of as simply authorisers
// And have no functionality except to channel funciton calls
public class PlayerCommands : NetworkBehaviour {

	// Damages another player, which is ran on the server
	[Command]
	public void CmdgiveDamage (GameObject otherPlayer, int damage){
		HealthManager otherHM = otherPlayer.GetComponent<HealthManager>();
		otherHM.damagePlayer (damage);
	}

	// respawns another player, for each of the clients
	[Command]
	public void CmdRespawnAnotherPlayer(GameObject otherPlayer, Vector3 newSpawnLoc){
		HealthManager playerHealthScript = otherPlayer.GetComponent<HealthManager>();
		playerHealthScript.RpcRespawn (newSpawnLoc);
	}

	// Scores another player, for each of the clients
	[Command]
	public void CmdScorePlayers(GameObject playerWhoKilled, GameObject playerWhoDied) {
		GameObject theControllerHouse = (GameObject)GameObject.Find ("Controller");
		GameController control = theControllerHouse.GetComponent<GameController> ();
		control.RpcScorePlayers (playerWhoKilled, playerWhoDied);
	}

	// this is only ever called on the server
	// The team and player values are synced with the local copy of each
	// players scripts
	[Command]
	public void CmdsetPlayerTeamAndIdent(GameObject player) {
		GameObject theControllerHouse = (GameObject)GameObject.Find ("Controller");
		PlayerIdent playerManager = theControllerHouse.GetComponent<PlayerIdent> ();
		TeamIdent teamManager =  theControllerHouse.GetComponent<TeamIdent> ();
		int newPlayerNum = playerManager.getNewId ();
		int newPlayerTeamNum = teamManager.getTeam();
		PlayerTeam playerTeam = player.GetComponent<PlayerTeam> ();
		playerTeam.setPlayerNum (newPlayerNum);
		playerTeam.setTeamNum (newPlayerTeamNum);
	}
}
