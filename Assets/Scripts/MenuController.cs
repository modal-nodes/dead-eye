﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


//Controller to render the main menu.
public class MenuController : MonoBehaviour {
	//Static class vars
	private static int w = Screen.width;
	private static int h = Screen.height;
	//Styling
	private GUIStyle buttonStyle;

	//Run once at the start
	public void Start() {
		//This should lock the screen to landscape on Android.
		Screen.autorotateToLandscapeLeft = true;
		Screen.autorotateToLandscapeRight = true;
		Screen.autorotateToPortrait = false;
		Screen.autorotateToPortraitUpsideDown = false;
		Screen.orientation = ScreenOrientation.AutoRotation;
	}

	//Renders all the graphics
	public void OnGUI() {
		//Styling
		buttonStyle = new GUIStyle(GUI.skin.GetStyle("box"));
		buttonStyle.fontSize = 60; //change the font size
		buttonStyle.alignment = TextAnchor.MiddleCenter;

		//Render the buttons
		if (GUI.Button (new Rect (w / 10, 3 * h / 10, w / 3, h / 6), "Play Game", buttonStyle)) {
			//if (NetworkController) NetworkController.SetActive (true);
			LoadScene ("MainMap");
		}
		if (GUI.Button( new Rect(6*w/10, 6*h/10, w/3, h/6), "Settings", buttonStyle)) 
			LoadScene("Settings");
		if (GUI.Button( new Rect(6*w/10, 3*h/10, w/3, h/6), "Stats", buttonStyle)) 
			LoadScene("Stats");
		if (GUI.Button( new Rect(w/10, 6*h/10, w/3, h/6), "Exit", buttonStyle)) 
			Application.Quit();
	}
		
	//Function to switch scenes. 
	public void LoadScene(string sceneName) {
		SceneManager.LoadScene (sceneName);
	}
}
