﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using System.Collections.Generic;

public class SimpleMatchMaker : MonoBehaviour
{ 
	// https://docs.unity3d.com/Manual/UpgradeGuide54Networking.html
	NetworkMatch matchMaker;
	void Awake()
	{	
		Debug.Log ("MM awake");
		matchMaker = gameObject.AddComponent<NetworkMatch>();
	}
	private void createMatch() {
		matchMaker.CreateMatch("roomName", 4, true, "", "", "", 0, 0, OnMatchCreate);
	}

	// Called when match is created 
	public void OnMatchCreate(bool success, string extendedInfo, MatchInfo matchInfo)
	{
		
	}

	public void listMatches () {
		matchMaker.ListMatches (0, 10, "", true, 0, 0, OnMatchList);
	}

	public void OnMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
	{

	}

	public void joinMatch() {
		//matchMaker.JoinMatch(networkId, "" , "", "", 0, 0, OnMatchJoined);
	}

	public void OnMatchJoined(bool success, string extendedInfo, MatchInfo matchInfo)
	{
	}
	// Destroy match
	//matchMaker.DestroyMatch(netId, 0, OnMatchDestroy);

	// Callback
	public void OnMatchDestroy(bool success, string extendedInfo)
	{

	}

	/*

	void Start()
	{
		NetworkManager.singleton.StartMatchMaker();
	}

	public override void OnMatchCreate(bool success, string extendedInfo, MatchInfo matchInfo)
	{
		_currentMatchID = (ulong)matchInfo.networkId;

		base.OnMatchCreate(success, extendedInfo, matchInfo);
	}


	//call this method to request a match to be created on the server
	public void CreateInternetMatch(string matchName)
	{
		CreateMatchRequest create = new CreateMatchRequest();
		create.name = matchName;
		create.size = 4;
		create.advertise = true;
		create.password = "";

		NetworkManager.singleton.matchMaker.CreateMatch(create, OnInternetMatchCreate);
	}

	//this method is called when your request for creating a match is returned
	private void OnInternetMatchCreate(CreateMatchResponse matchResponse)
	{
		if (matchResponse != null && matchResponse.success)
		{
			//Debug.Log("Create match succeeded");

			MatchInfo hostInfo = new MatchInfo(matchResponse);
			NetworkServer.Listen(hostInfo, 9000);

			NetworkManager.singleton.StartHost(hostInfo);
		}
		else
		{
			Debug.LogError("Create match failed");
		}
	}

	//call this method to find a match through the matchmaker
	public void FindInternetMatch(string matchName)
	{
		NetworkManager.singleton.matchMaker.ListMatches(0, 20, matchName, OnInternetMatchList);
	}

	//this method is called when a list of matches is returned
	private void OnInternetMatchList(ListMatchResponse matchListResponse)
	{
		if (matchListResponse.success)
		{
			if(matchListResponse.matches.Count != 0)
			{
				//Debug.Log("A list of matches was returned");

				//join the last server (just in case there are two...)
				NetworkManager.singleton.matchMaker.JoinMatch(matchListResponse.matches[matchListResponse.matches.Count - 1].networkId, "", OnJoinInternetMatch);
			}
			else
			{
				Debug.Log ("No matches in requested room!");
			}
		}
		else
		{
			Debug.LogError("Couldn't connect to match maker");
		}
	}

	//this method is called when your request to join a match is returned
	private void OnJoinInternetMatch(JoinMatchResponse matchJoin)
	{
		if (matchJoin.success)
		{
			//Debug.Log("Able to join a match");


			MatchInfo hostInfo = new MatchInfo(matchJoin);
			NetworkManager.singleton.StartClient(hostInfo);
		}
		else
		{
			Debug.LogError("Join match failed");
		}

		
	}*/
}