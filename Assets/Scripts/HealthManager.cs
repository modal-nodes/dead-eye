﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class HealthManager : NetworkBehaviour {
	// Textures for the health manager
	// For the health bar
	public Texture greenTexture;
	public Texture redTexture;
	// For the respawning screen
	public Texture redOverlayTexture;

	// Amount of time since the player was damaged
	private float timeSinceDamaged;
	// Amount of time since the player regenerated any health
	private float timeSinceLastRegen;
	// Amount of time needed to pass before player can respawn
	// Assuming they have not been damaged in that time
	private float timeThreshold;
	// Amount of time between regenerations
	private float timeThresholdForRegen;

	private int maxHealth;
	// How much health is gained on each regeneration
	private int healthRegenAmount;

	// On change of the value, HealthChanged is called
	[SyncVar(hook = "HealthChanged")]
	private int currentHealth;

	// On change of the value, AliveChanged is called
	[SyncVar(hook = "AliveChanged")]
	private bool isAlive = true;
	// Amount of time since the player last died
	private float timeSinceDeath = 0;

	private GameRules gamerules;
	private Scoring scoring;

	// The next spawn location to be used, after the player has waiting the
	// required timeThreshold (above)
	private Vector3 nextSpawnLoc;


	// This must be start, not Awake.
	// As the controller sets up it's game state first
	// Else we might lose out on a race condition
	void Start() {
		gamerules = new GameRules ();

		if (isLocalPlayer) {
			// Hide the player from local view
			// To avoid the hat rim showing when panning down
			gameObject.transform.FindChild ("char02").gameObject.SetActive (false);
			greenTexture = (Texture)Resources.Load ("green");
			redTexture = (Texture)Resources.Load ("red");
			redOverlayTexture = (Texture)Resources.Load ("redOverlay");
			timeSinceDamaged = 0f;
			timeSinceLastRegen = 0f;
			GameObject theControllerHouse = (GameObject)GameObject.Find ("Controller");
			// The Controller gameObject was disabling for the client, due to network issues
			theControllerHouse.SetActive (true);
			GameController control = theControllerHouse.GetComponent<GameController> ();

			scoring = theControllerHouse.GetComponent<Scoring> ();
			// register the player with the GameController
			// In order to set up team and player indentifiers
			control.addPlayer (this.transform.gameObject);
		}
	}

	// On health change, reset the timers
	private void HealthChanged(int newHealth) {
		int oldHealth = currentHealth;
		currentHealth = newHealth;
		if (oldHealth > newHealth) {
			// We just regenerated
			timeSinceLastRegen = 0;
		} else {
			// We just took damage
			timeSinceDamaged = 0;
		}
	}

	// The gamecontroller has changed our state
	public void AliveChanged(bool newState) {
		if (!isLocalPlayer) {
			return;
		}
		timeSinceDeath = 0;
		isAlive = newState;
		// Also, tell playerMotor, so that controls can be enabled / disabled
		PlayerMotor pm = GetComponent<PlayerMotor>();
		pm.setisAlive (newState);

		if (newState == true) {
			// we're alive again, Need to respawn

		} else {
			// We just died. Need to spawn a ragdoll in our position
			CmdspawnRagdoll (transform.position, transform.rotation);
		}
	}

	// Executed on the server, it will tell each client to spawn a ragdoll
	[Command]
	public void CmdspawnRagdoll(Vector3 playerPos, Quaternion playerRot){
		Debug.Log ("Dropping Ragdoll");
		// Need to spawn the ragdoll slightly higher
		// Else it collides with the ground
		Vector3 ragdollPos = playerPos;
		ragdollPos.y += 1f;

		// this is needed to ensure the ragdoll is spawned correctly above the map
		// We're loosing a race condition with other networking issues
		ragdollPos.y = Mathf.Clamp(ragdollPos.y, 1,ragdollPos.y );
		RpcSpawnRagdolls (ragdollPos, playerRot);

	}

	// Spawn a ragdoll on each client so every player can see 
	// where dead bodies are 
	[ClientRpc]
	public void RpcSpawnRagdolls(Vector3 ragdollPos, Quaternion playerRot) {
		GameObject dead_body =(GameObject) Instantiate(
			Resources.Load("PlayerRagdoll"), ragdollPos, playerRot);
	}

	// Damages a player only if the player is not marked as !isAlive
	public void damagePlayer(int dam) {
		if (isAlive) {
			timeSinceDamaged = 0;
			currentHealth -= dam;
		}
	}

	public void setCurrentHealth(int newHealth) {
		currentHealth = newHealth;
	}


	// This function is needed for self harm.
	// It changes the health on the server, and on the local player in term
	// Via the syncvars
	[Command]
	public void CmdDamagePlayer(int dam) {
		if (isAlive) {
			Debug.Log ("Where we!");
			timeSinceDamaged = 0;
			currentHealth -= dam;
			if (currentHealth < 0) {
				RpcRespawn (new Vector3 (0, 2, 0));

				//Scores a suicide as death
				scoring.scoreDeath (gameObject, gameObject);
			}
		}
	}

	// Called only by the server to tell the local player they are dead.
	public void killOffPlayer(){
		isAlive = false;
		timeSinceDeath = 0;
	}

	// Respawn a player on each client
	[ClientRpc]
	public void RpcRespawn(Vector3 newLoc) {
		if (isServer) {
			killOffPlayer ();
		}

		// Moves the player out of view
		if (isLocalPlayer) {
			// Set player motors is alive!
			// Else a race condition happens, the cam moves before
			// The player is locked

			PlayerMotor pm = GetComponent<PlayerMotor>();
			pm.setisAlive (false);
			//CmdIncreaseHealth(maxHealth);

			// Hide the player below the map whilst they respawn
			transform.position = new Vector3 (0, -5, 0);
		}
		// Store the next location in the player
		setNextSpawnPos (newLoc);
		//currentHealth = maxHealth;
	}

	// Sets the location the player will spawn too after the waiting time
	public void setNextSpawnPos(Vector3 pos) {
		nextSpawnLoc = pos;
	}

	public void respawn(Vector3 newLoc) {
		// Can do this, as this is only called by the server
		currentHealth = maxHealth;
		transform.position = newLoc;
	}

	void OnGUI()
	{
		if (! isLocalPlayer) {
			return;
		}
		if (isAlive) {
			makeHealthBar ();
		} else {
			makeDeathWarning ();
		}
	}

	// GUI function to show a death warning, and respawn time
	private void makeDeathWarning() {
		GUIStyle textStyle = new GUIStyle ();
		textStyle.fontSize = 30;
		textStyle.alignment = TextAnchor.MiddleCenter;
		textStyle.fontStyle = FontStyle.Bold;
		textStyle.normal.textColor = Color.black;

		int timeToLiveAgain = (int) (gamerules.getTimeToRespawn () - timeSinceDeath);

		// All hard coded numbers, that displayed well on a mobile
		int textWidth = 300;
		int textHeight = 20;

		// Set the positon according to the screen size
		int xCord = (Screen.width / 2) - (textWidth /2);
		int yCord = Screen.height / 2;


		// Gap between the text and the countdown
		int margin = 20;

		GUI.DrawTexture ((new Rect (0, 0, Screen.width, Screen.height)), redOverlayTexture, ScaleMode.StretchToFill);
		GUI.TextField(new Rect(xCord, yCord, textWidth, textHeight), "YOU DIED!", textStyle);
		GUI.TextField(new Rect(xCord, yCord + (textHeight + margin), textWidth, textHeight), "Respawn in :", textStyle);
		GUI.TextField(new Rect(xCord, yCord + (2* (textHeight + margin)), textWidth, textHeight),timeToLiveAgain.ToString() , textStyle);
	}

	// Executed by the server to check that the player is alive again
	[Command]
	public void CmdMakePlayerAlive() {
		isAlive = true;
		currentHealth = maxHealth;

	}

	// Show a health bar at the top of the screen
	public void makeHealthBar (){
		float barWidth = 200f;
		float barHeight = 20f;

		// Set the current health to 0 if the health is a minus value
		int currentHealthorZero = Mathf.Max (currentHealth, 0);

		// the width of the green vs red bars
		float greenWidth = barWidth * ((float)currentHealthorZero / (float)maxHealth);
		float redWidth = barWidth - greenWidth;

		// Offset from the top of the screen
		float offset = 10;

		// The leftmost x point for the green bar

		float xGreen = (Screen.width / 2) - (barWidth / 2);
		float xRed = xGreen + greenWidth;

		GUI.DrawTexture ((new Rect (xGreen, offset, greenWidth, barHeight)), greenTexture, ScaleMode.StretchToFill);
		GUI.DrawTexture ((new Rect (xRed, offset, redWidth, barHeight)), redTexture, ScaleMode.StretchToFill);
	}

	void Update()
	{
		if (!isLocalPlayer){
			return;
		}
		checkIfNeedToRegen ();

		if (!isAlive) {
			timeSinceDeath += Time.deltaTime;
			if (timeSinceDeath > gamerules.getTimeToRespawn ()) {
				// Tell the server we want to live again!
				CmdMakePlayerAlive();
				// Change the player position to the respawn point
				this.transform.position = nextSpawnLoc;
			}
		}
	}

	private void checkIfNeedToRegen()
	{
		if (!isLocalPlayer) {
			return;
		}
		if (currentHealth == maxHealth) {
			return;
		}

		if (!isAlive) {
			return;
		}

		// Check if enough time has passed to respawn
		timeSinceDamaged += Time.deltaTime;
		timeSinceLastRegen += Time.deltaTime;
		if (timeSinceDamaged >= timeThreshold) {
			if (timeSinceLastRegen >= timeThresholdForRegen) {
				regenerateHealth ();
			}
		}
	}

	void regenerateHealth()
	{
		timeSinceLastRegen = 0;
		int newHealth = currentHealth + healthRegenAmount;
		// Dont allow the health to be over maxHealth
		newHealth = Mathf.Min (maxHealth, newHealth);
		// Register this increase with the server
		CmdIncreaseHealth (newHealth);
	}

	// Tell the server to increase our health
	// Which will change on the local copy through the syncvar
	[Command]
	public void CmdIncreaseHealth(int newHealth) {
		currentHealth = newHealth;
	}

	public int getCurrentHealth()
	{
		return currentHealth;
	}

	public void setMaxHealth(int mh){
		maxHealth = mh;
	}

	public int getMaxHealth(){
		return maxHealth;
	}

	[Command]
	public void CmdsetCurrentHealth(int hp)
	{
		currentHealth = hp;
		//Death Check to fix replay
		if (currentHealth <= 0) {
			RpcRespawn (new Vector3 (0, 2, 0));
		}
	}

	public void setHealthRegenAmount(int amount) {
		healthRegenAmount = amount;
	}
	public void setTimeThreshold(float newThresh) {
		timeThreshold = newThresh;
	}
	public void setTimeThresholdForRegen(float newThresh) {
		timeThresholdForRegen = newThresh;
	}
}
