﻿using UnityEngine;
using System.Collections;

public class LobbyController : MonoBehaviour {

	ImplementedNetworkManager manager;
	int w = Screen.width, h = Screen.height;

	string ip_address = "Enter Host-IP";

	private bool in_game=false;
	private static GUIStyle buttonStyle,fieldStyle;

	// Retrieve the Network Manager object
	void Awake () {
		GameObject overall_object = GameObject.Find ("Network Manager");
		manager = overall_object.GetComponent<ImplementedNetworkManager> ();

	}

	// Build buttons when Lobby is created dependant on ratios
	// of the screen width and height to support multi platform
	// /different resolutions and aspect ratio devices
	void OnGUI() {

		buttonStyle = new GUIStyle (GUI.skin.GetStyle("button"));
		buttonStyle.fontSize = 50;
		buttonStyle.alignment = TextAnchor.MiddleCenter;

		fieldStyle = new GUIStyle (GUI.skin.GetStyle("textField"));
		fieldStyle.fontSize = 50;
		buttonStyle.alignment = TextAnchor.MiddleCenter;


		if (!manager.isNetworkActive && !in_game) {
			if (GUI.Button (new Rect (6 * w / 10, 6 * h / 10, w / 6, h / 3), "Host", buttonStyle)) {
				if (manager.StartHost ().isConnected)
					in_game = true;
			}
			ip_address = GUI.TextField (new Rect (6 * w / 10, 6 * h / 10 - 2*h / 4, w / 3, h/6), ip_address, fieldStyle);
			if (GUI.Button (new Rect (8 * w / 10, 6 * h / 10, w / 6, h / 3), "Join",buttonStyle)) {
				manager.networkAddress = ip_address;
				if (manager.StartClient ().isConnected) {
					in_game = true;
				}
			}
		}
	}

}
