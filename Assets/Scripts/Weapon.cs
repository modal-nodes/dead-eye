﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

[System.Serializable]
public class Weapon : NetworkBehaviour {

	//Initialised values

	public int ammoCapacity;
	public float fireRate;
	public float maxDamage;
	public float reloadTime;
	public string weaponName;
	//public float sphereCastRadius;
	public float range;

	private bool isReloading = false;
	private int currentAmmo;
	private float timeSinceFired = fireRate;

	private AudioManager am;

	private float force = 200;


	//Constructor
	public Weapon (int ammoCapacity, float fireRate, float maxDamage, float reloadTime, string weaponName)
	{
		this.ammoCapacity = ammoCapacity;
		this.fireRate = fireRate;
		this.maxDamage = maxDamage;
		this.reloadTime = reloadTime;
		this.weaponName = weaponName;

	}

	void Awake ()
	{
		currentAmmo = ammoCapacity;
		am = GetComponentInParent<AudioManager> ();
		timeSinceFired = fireRate;
	}

	// When the fire button is played, plays the correct sound
	// and upadges the state of the gun, ie whether it's reloading etc
	public void fire ()
	{
		if (isReloading || timeSinceFired < fireRate) {
			return;
		}
		am.playSound (this.weaponName);
		currentAmmo--;
		timeSinceFired = 0;
		if (currentAmmo == 0) {
			StartCoroutine (reload ());
			isReloading = true;
		}
	}

	[Command]
	private void CmdPlayFireSound() {
		RpcPlayFireSound ();
	}

	[ClientRpc]
	private void RpcPlayFireSound() {
		//ac.PlayOneShot (gunshotSound);
	}

	// Update the time since fired, to check reloading
	void Update() {
		timeSinceFired += Time.deltaTime;
	}

	//Watch out with this for networking
	IEnumerator reload()
	{
		yield return new WaitForSeconds (0.15f);

		reloadStart ();
		yield return new WaitForSeconds (reloadTime);
		reloadDone ();

		//Wait a tiny bit longer for reload_done sound to play
		yield return new WaitForSeconds (0.15f);
		refilAmmo ();
		isReloading = false;

	}

	// Play the sound, and show the animations for reload
	private void reloadStart() {
		am.playSound ("reload_start");
		this.transform.Rotate (0, -30, 0);
	}

	// Play the sound, and show the animations for finishing reloading
	private void reloadDone() {
		this.transform.Rotate (0, 30, 0);
		am.playSound ("reload_done");
	}


	// Calculates the amount of damage a weapon should deal,
	// Given a distance
	public int calculateDamage(float distance)
	{
		//If we are a shotgun, we do high damage at close range, and low damage at a distance
		if (this.weaponName == "Shotgun") {
			return (int) ((1 / distance) * maxDamage);
		}

		//If we are a pistol or rifle, distance doesn't matter as much
		return (int) (maxDamage - distance);
	}

	public void refilAmmo() {
		this.currentAmmo = ammoCapacity;
	}

	// the range a weapon can still deal damage at
	public float getRange()
	{
		return this.range;
	}

	// get the force the shot of a weapon deals 
	public float getForce()
	{
		return this.force;
	}

	public int getMaxAmmo()
	{
		return this.ammoCapacity;
	}

	public int getCurrentAmmo()
	{
		return this.currentAmmo;
	}

	public bool getIsReloading() {
		return isReloading;
	}

	public bool getCanFire() {
		return (timeSinceFired > fireRate);
	}



}
