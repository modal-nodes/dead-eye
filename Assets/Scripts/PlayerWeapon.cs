using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class PlayerWeapon : NetworkBehaviour
{
	private Camera cam;
	private Dictionary<string, GameObject> weaponsDict;
	private Weapon currentWeapon;

	private const string DEFAULT_WEAPON = "Pistol";

	// Use this for initialization
	void Start ()
	{
		if (!isLocalPlayer)
			return;

		cam = Camera.main;
		makeWeaponsDict ();
		setDefaultWeapon ();

	}

	// The fire button has been pressed
	public void fire(){
		currentWeapon.fire ();

		if (currentWeapon.getIsReloading())
			return;
		if (currentWeapon.getCanFire ())
			return;

		// Create a ray from the camera, see if it hits anything
		Ray ray = new Ray (cam.transform.position, cam.transform.forward);
		RaycastHit hitInfo;

		// Max distance of the raycast is 100
		if (Physics.Raycast (ray, out hitInfo, 100)) {
			Vector3 hitPoint = hitInfo.point;

			// The gameObject that has been hit
			GameObject go = hitInfo.collider.gameObject;

			if (go.tag == "Player") {
				// We've hit another player
				// Damage them
				PlayerController otherScript = go.GetComponent<PlayerController>();

				float distance = Vector3.Distance (this.transform.position, go.transform.position);
				//Calculate damage amount based on weapon stats
				int damageAmount = currentWeapon.calculateDamage (distance);

				//Damage the player, through the gameController
				GameObject theControllerHouse = (GameObject)GameObject.Find("Controller");
				GameController control = theControllerHouse.GetComponent<GameController> ();

				control.hit (this.gameObject, go, damageAmount);

			}

		}
	}

	// Reload the weapon
	public void refilAmmo() {
		this.currentWeapon.refilAmmo();
	}


	//This method builds the dictionary of available weapons
	private void makeWeaponsDict() {
		//Make dictionary
		weaponsDict = new Dictionary<string, GameObject> ();

		GameObject pistol = this.transform.FindChild ("Pistol").gameObject;
		Debug.Log (pistol);
		GameObject shotgun = this.transform.FindChild ("Shotgun").gameObject;
		GameObject rifle = this.transform.FindChild ("Rifle").gameObject;

		//Add weapons from resource folder
		weaponsDict.Add ("Pistol", pistol);
		weaponsDict.Add ("Shotgun", shotgun);
		weaponsDict.Add ("Rifle", rifle);

	}

	//This method sets the player's default weapon
	public void setDefaultWeapon() {
		equipWeapon (DEFAULT_WEAPON);

	}

	public int getCurrentAmmo() {
		return this.currentWeapon.getCurrentAmmo();
	}

	public int getMaxAmmo() {
		return this.currentWeapon.getMaxAmmo();
	}

	public string getCurrentWeapon() {
		return currentWeapon.weaponName;
	}

	[Client]
	//This method equips a new weapon
	public void equipWeapon(string weaponName) {

		if (currentWeapon) {
			CmdUnequipWeapon (currentWeapon.weaponName);
		}
		//Current weapon script is set
		currentWeapon = this.transform.FindChild (weaponName).gameObject.GetComponent<Weapon>();
		refilAmmo ();
		CmdEquipWeapon (weaponName);

	}

	[Client]
	private void unEquipWeapon() {
		CmdUnequipWeapon (this.currentWeapon.weaponName);
		this.currentWeapon = null;
	}

	// Run on the server, to call below
	[Command]
	private void CmdEquipWeapon(string weaponName) {
		RpcEquipWeapon (weaponName);
	}

	// Ensures the weapon equipped is correct on all clients
	[ClientRpc]
	private void RpcEquipWeapon(string weaponName) {
		GameObject equippedWeapon = this.transform.FindChild (weaponName).gameObject;
		MeshRenderer mr = equippedWeapon.GetComponent<MeshRenderer> ();
		mr.enabled = true;
	}

	// Ran on the server, calls below
	[Command]
	private void CmdUnequipWeapon(string weaponName) {
		RpcUnequipWeapon (weaponName);
	}

	// Ensures the weapon unequipped is correct on all clients
	[ClientRpc]
	private void RpcUnequipWeapon(string weaponName) {
		GameObject currentWeapongo = this.transform.FindChild (weaponName).gameObject;
		MeshRenderer mr = currentWeapongo.GetComponent<MeshRenderer> ();
		mr.enabled = false;
	}
}
