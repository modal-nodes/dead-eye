﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : NetworkBehaviour {

	private AudioSource ac;
	// This is a map between a name and an audioclip for easier lookup
	public Dictionary<string, AudioClip> audioDictionary;

	// Use this for initialization
	void Start () {

		ac = this.GetComponent<AudioSource> ();
		audioDictionary = makeAudioDict ();

	}

	public void playSound(string soundName) {
		if (!isLocalPlayer) {
			return;
		}

		if (audioDictionary.ContainsKey (soundName)) {
			// call the server, below
			CmdPlaySound (soundName);
		} else {
			Debug.Log ("Sound not in dict");
		}
	}

	// get the server to play the sound on all players
	[Command]
	private void CmdPlaySound(string soundName) {
		RpcPlaySound (soundName);
	}

	// This runs on each client, to play the sound at the correct position
	[ClientRpc]
	private void RpcPlaySound(string soundName) {
		AudioClip sound = audioDictionary [soundName];
		AudioSource.PlayClipAtPoint (sound, this.transform.position);
	}

	//This method creates the audio dictionary by loading clips
	//From the resources folder
	//ALL SOUND EFFECTS SOURCED FROM https://www.freesound.org/
	private Dictionary<string, AudioClip> makeAudioDict() {
		Dictionary<string, AudioClip> adict = new Dictionary<string, AudioClip> ();

		//Load audioclips
		AudioClip pistol = Resources.Load ("SoundFX/pistol_fire") as AudioClip;
		AudioClip rifle = Resources.Load ("SoundFX/rifle_fire") as AudioClip;
		AudioClip shotgun = Resources.Load ("SoundFX/shotgun_fire") as AudioClip;
		AudioClip reload_start = Resources.Load ("SoundFX/reload_start") as AudioClip;
		AudioClip reload_done = Resources.Load ("SoundFX/reload_done") as AudioClip;
		AudioClip hit_soft = Resources.Load ("SoundFX/hit_soft") as AudioClip;
		AudioClip hit_hard = Resources.Load ("SoundFX/hit_hard") as AudioClip;
		AudioClip death = Resources.Load ("SoundFX/death") as AudioClip;

		//Add to dictionary
		adict.Add("Pistol", pistol);
		adict.Add ("Rifle", rifle);
		adict.Add ("Shotgun", shotgun);
		adict.Add ("reload_start", reload_start);
		adict.Add ("reload_done", reload_done);
		adict.Add ("death", death);

		return adict;

	}
}
