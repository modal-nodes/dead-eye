﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;
using System.IO;
using System.Collections;


//This class acts as the central controller for the player. It handles input, then sends messages to the 
//Required components each frame
public class PlayerController : NetworkBehaviour {
	public PlayerMotor motor;
	public PlayerWeapon playerweapon;
	public HealthManager health;
	public PlayerReplay replay;

	//Bool to check which input method is being used
	private bool isAndriod = true;

	private GameController gc;

	//Button and replay check bools
	private bool jumpPressed = false;
	private bool shootPressed = false;
	private bool isPlayback = false;
	private bool isPlaybackStart = true;

	public GUIStyle buttonStyle, buttonStyleSmall;

	private Vector3 defaultPosition;

	//For displaying GUI
	public Text currentAmmoText;
	public Text maxAmmoText;

	//Needed for replay
	private int oldHealth;

	// Screen paramaters
	int screenWidth = Screen.width;
	private static int buttonWidth = Screen.width/5;
	private int screenXDivide = (Screen.width - buttonWidth) / 2;
	static int screenHeight = Screen.height;
	int buttonHeight =  screenHeight / 4;

	//GameOver check bool
	private bool isGameOver = false;

	void OnGUI(){
		if (!isLocalPlayer)
			return;
		createButtons();
		displayAmmo ();
	}

	void Start() {
		//Get components
		motor = GetComponent<PlayerMotor> ();
		playerweapon = GetComponent<PlayerWeapon> ();
		health = GetComponent<HealthManager> ();
		replay = GetComponent<PlayerReplay> ();
		gc = GameObject.Find ("Controller").GetComponent<GameController> ();

		//Set default position
		defaultPosition = this.transform.position;

		//For UI stuff
		if (isLocalPlayer) {
			//Display Ammo GUI elements
			GameObject currentAmmogo = GameObject.Find ("Canvas/CurrentAmmo");
			GameObject maxAmmogo = GameObject.Find ("Canvas/MaxAmmo");
			currentAmmoText = currentAmmogo.GetComponent<Text> ();
			maxAmmoText = maxAmmogo.GetComponent<Text> ();

			// Make own local player invisible, to remove the hat rim issue
			GameObject char02 = transform.GetChild(1).gameObject;
			char02.SetActive (false);
			Debug.Log (char02.name);
		}


		if (isAndriod) {
			//This should lock the screen to landscape on Android.
			Screen.autorotateToLandscapeLeft = true;
			Screen.autorotateToLandscapeRight = true;
			Screen.autorotateToPortrait = false;
			Screen.autorotateToPortraitUpsideDown = false;
			Screen.orientation = ScreenOrientation.AutoRotation;
		}
		else {
			//Cursor.lockState = CursorLockMode.Locked;
		}

		//Sets the inital buffer position to 0
	}

	void Update() {

		if (!isLocalPlayer){
			return;
		}

		//Boolean to check if it's playing back or not
		if (!isPlayback) {

			//If not, handle input
			if (isAndriod) {
				bool wasMoved = false;
				bool wasRotated = false;
				if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Moved) {
					// Go through each touch
					for (int i = 0; i < Input.touchCount; i++) {
						Vector2 deltaTouch = Input.GetTouch (i).deltaPosition;
						if (Input.GetTouch (i).position.x < screenXDivide) {
							// Is movement for the player, left hand part of screen
							Vector3 v3Touch = new Vector3 (deltaTouch.x, 0, deltaTouch.y);
							motor.setMoveVector (v3Touch);
							replay.ic.setSpeed (v3Touch);
							wasMoved = true;

							// Check if the player is scrolling on the buttons
						} else if (Input.GetTouch (i).position.x < screenWidth - buttonWidth) {
							// Is rotation for player, right hand part of screen
							motor.rotatePlayer (-deltaTouch.x, deltaTouch.y);
							replay.ic.setRotateX (-deltaTouch.x);
							replay.ic.setRotateY (deltaTouch.y);
							wasRotated = true;

						} else {
							// Button pressed, find which one
							if (Input.GetTouch(i).position.y < (screenHeight / 4)) {
								//Chat
							}
							else if (Input.GetTouch(i).position.y < ((2* screenHeight) / 4)) {
								// Map
							}
							else if (Input.GetTouch(i).position.y < ((3* screenHeight) / 4)) {
								// jump
								jumpPressed = true;
							}
							else  {
								// fire
								shootPressed = true;
							}
						}
					}

				}
				// If no rotation/movement detected this frame, record null vectors to ensure consistency
				if (!wasMoved) {
					motor.setMoveVector(new Vector3(0,0,0));
					replay.ic.speed = new Vector3 (0, 0, 0);
				}
				if (!wasRotated) {
					replay.ic.rotateX = 0;
					replay.ic.rotateY = 0;
				}


			} else {
				if ((Input.GetAxis ("Vertical") != 0) | (Input.GetAxis ("Horizontal") != 0)) {

					//Directional Input
					float forwardSpeed = Input.GetAxis ("Vertical");
					float strafeSpeed = Input.GetAxis ("Horizontal");


					//Set speed vector
					Vector3 speed = new Vector3 (5* strafeSpeed, 0, 5* forwardSpeed);
					motor.setMoveVector (speed);
					replay.ic.speed = speed;
				} else {
					motor.setMoveVector (new Vector3 (0, 0, 0));
					replay.ic.speed = new Vector3 (0, 0, 0);
				}
			}

			//Record jump and shoot
			replay.ic.shoot = shootPressed;
			replay.ic.jump = jumpPressed;

			//Check for jumping and shooting actions, as they need to be recorded
			//If they are pressed, then go ahead and shoot/jump. This ensures consistency in updating


			if (jumpPressed) {
				motor.jump ();
				jumpPressed = false;
			}

			if (shootPressed) {
				playerweapon.fire ();
				shootPressed = false;
			}

			//Record health
			replay.ic.health = health.getCurrentHealth ();

			//Finally, save the input of this frame
			replay.SaveState ();

			//Else, it's replaying right now
		} else {

			if (isPlaybackStart) {
				//Playback is just starting, so set defaults and start replay
				setPlayerDefaults ();
				isPlaybackStart = false;
			}

			//Load frame data from PlayerReplay
			replay.LoadFrame ();
			//Play frame back in engine
			playFrame ();
		}

	}

	public void gameOver () {
		if (!isLocalPlayer) {
			return;
		}
		Debug.Log ("GAME OVER");
		isGameOver = true;

		// Stop movement of the player
		//motor.setIsGameOver (true);
		Debug.Log ("Starting timer");
		//Wait for objects to respawn, health to regenerate etc
		StartCoroutine(	WaitSomeTime (6));

	}

	// Waits some time, then sets isPlayback to true
	IEnumerator WaitSomeTime(int seconds) {
		yield return new WaitForSeconds(seconds);
		Debug.Log ("Finished timer");
		//Start replay
		isPlayback = true; 
	}

	public void createButtons(){
		buttonStyle = new GUIStyle(GUI.skin.GetStyle("box"));
		buttonStyle.fontSize = 50; //change the font size
		buttonStyle.alignment = TextAnchor.MiddleCenter;
		// buttonStyleSmall = buttonStyle; 
		// buttonStyleSmall.fontSize = 32; 

		if (GUI.Button(new Rect(screenWidth - buttonWidth,screenHeight - buttonHeight,buttonWidth,buttonHeight), "Fire", buttonStyle)){
			//Debug.Log("Fire");
			shootPressed = true;
		}
		if (GUI.Button(new Rect(screenWidth - buttonWidth,screenHeight - 2*buttonHeight,buttonWidth,buttonHeight), "Jump", buttonStyle)){
			//Debug.Log("Jump");
			jumpPressed = true;
		}
		if (GUI.Button(new Rect(screenWidth - buttonWidth,screenHeight - 3*buttonHeight,buttonWidth,buttonHeight), "Map", buttonStyle)){
			//Debug.Log("Map");
		}
		if (GUI.Button (new Rect (250, 0, 100, 100), "replay")) {
			//this.isPlayback = true;
			CmdStartReplayNetworked();
		}
	}

	//This method displays the player's ammunition stats on the GUI
	private void displayAmmo() {

		currentAmmoText.text = playerweapon.getCurrentAmmo ().ToString ();
		maxAmmoText.text = playerweapon.getMaxAmmo ().ToString ();
	}

	//This function plays back the next frame in the Replay MemoryStream
	private void playFrame() {

		//Perform rotation, jumping, shooting and movement.
		motor.rotatePlayer (replay.ic.rotateX, replay.ic.rotateY);
		if (replay.ic.jump) {
			motor.jump ();
		}
		if (replay.ic.shoot) {
			playerweapon.fire ();
		}
		motor.movePlayer (replay.ic.speed);

		//Make them deal damage if there's a difference in recorded health
		if (oldHealth > replay.ic.health) {
			gc.selfHalm (this.gameObject, oldHealth - replay.ic.health);
		}

		//Record previous frame's health to check for a difference
		oldHealth = replay.ic.health;
	}

	//Sets player defaults for replay
	private void setPlayerDefaults() {
		this.transform.position = replay.getStartPosition ();
		this.transform.rotation = replay.getStartRotation ();
		this.playerweapon.equipWeapon (replay.getStartWeapon ());
	}

	//Command/ClientRPC to set playback status across network
	[Command]
	private void CmdStartReplayNetworked() {
		RpcStartReplayNetworked ();
	}

	[ClientRpc]
	private void RpcStartReplayNetworked() {
		this.isPlayback = true;
	}

}




