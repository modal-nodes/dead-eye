﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

//This script is attached to weapon pickups
public class WeaponPickup : NetworkBehaviour
{
	
	private Rigidbody rb;
	private MeshRenderer mr;
	private CapsuleCollider cc;
	public string weaponName;
	private const float SPEED = 1.0f;
	private const float HEIGHT = 1.3f;
	private const int DEG_PER_SEC = 1;

	private const float RESPAWN_TIME = 7.0f;

	void Start ()
	{

		rb = GetComponent<Rigidbody> ();
		mr = GetComponent<MeshRenderer> (); 
		cc = GetComponent<CapsuleCollider> ();
		
	}
		
	void FixedUpdate ()
	{
		//Apply horizontal rotation 
		this.transform.RotateAround(cc.bounds.center,  new Vector3(0, 1, 0), DEG_PER_SEC);


		//Apply vertical 'bob'
		if (transform.position.y < HEIGHT) {
			rb.AddForce (transform.up * SPEED);
		} else {
			rb.AddForce (-1 * transform.up * SPEED);
		}

	}

	//This function is used to determine whether a player is trying to pick up the weapon
	void OnTriggerEnter(Collider other) {
		
		if (other.gameObject.tag == "Player") {
			Debug.Log ("Entered trigger");
			StartCoroutine (weaponPickedUp ());
			equipWeaponOnPlayer (other.gameObject);
		}
	}

	private void equipWeaponOnPlayer(GameObject go) {
		PlayerWeapon pw = go.GetComponent<PlayerWeapon>();
		pw.equipWeapon(this.weaponName);
	}

	IEnumerator weaponPickedUp() {

		CmdDisableComponents ();
		yield return new WaitForSeconds (RESPAWN_TIME);
		CmdEnableComponents ();

	}

	[Command]
	void CmdDisableComponents() {
		RpcDisableComponents ();
	}

	[Command]
	void CmdEnableComponents() {
		RpcEnableComponents ();
	}

	[ClientRpc]
	void RpcDisableComponents() {
		this.rb.detectCollisions = false;
		this.mr.enabled = false;
	}

	[ClientRpc]
	void RpcEnableComponents() {
		this.rb.detectCollisions = true;
		this.mr.enabled = true;
	}


	
		
}