﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerTeam : NetworkBehaviour {
	// The current team number of the player
	[SyncVar]
	private int playerTeamNum;

	// The players current indentifier
	[SyncVar]
	private int playerNum;

	public void setTeamNum(int teamNum){
		playerTeamNum = teamNum;
	}

	public int getTeam(){
		return playerTeamNum;
	}

	public void setPlayerNum(int playerNum){
		this.playerNum = playerNum;
	}

	public int getPlayerNum(){
		return playerNum;
	}
}
