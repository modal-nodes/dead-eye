﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;


public class WeaponSpawner : NetworkBehaviour
{
	public GameObject shotgun;

	// Registers a prefab on client devices so 
	// clients see the given gameObject 
	public void OnStartClient() {
		ClientScene.RegisterPrefab (shotgun);
		Debug.Log ("Client starting");
		shotgun.transform.SetParent (this.transform);
	}

	// Spawns a new instance of a gun on the map. 
	[Server]
	public void ServerSpawnGun (Vector3 pos, Quaternion rot) {
		GameObject gun =(GameObject) Instantiate(Resources.Load("Shotgun"), pos, rot);
		gun.transform.SetParent (this.transform);

		NetworkServer.Spawn (gun);
		RpcSetParentForGun (gun, this.transform.gameObject);
	}

	// Sets parent object of a Gun spawned on the map
	[ClientRpc]
	public void RpcSetParentForGun(GameObject gun, GameObject parent){
		gun.transform.parent = parent.transform;
	}

	/*
	 *
	 *
	 * attachWeaponScript(weaponName, newWeapon);

		Weapon otherScript = newWeapon.GetComponent<Weapon>();


		Vector3 offSet = otherScript.getOffset();
		Quaternion initialRotation = otherScript.getInitialRoation();
		// Fix weapon rotation
		newWeapon.transform.localRotation = initialRotation;

		newWeapon.tag = "Weapon";
		newWeapon.transform.SetParent (this.transform);
		newWeapon.transform.position += offSet;
		newWeapon.isStatic = false;
	 *
	// Update is called once per frame
	[Command]
	void CmdspawnShotGun (string weaponType)
	{
		GameObject newWeapon;
		if (weaponType == "Shotgun") {
			newWeapon =
		}

		//ClientScene.RegisterPrefab (newWeapon);

		NetworkServer.Spawn (newWeapon);

	}
	*/
}
