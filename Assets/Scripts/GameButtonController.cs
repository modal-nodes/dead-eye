﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameButtonController : MonoBehaviour {

	private static int w = Screen.width;
	private static int h = Screen.height;
	GameObject NetworkController;
	ImplementedNetworkManager man;
	public GUIStyle buttonStyle;

	// Use this for initialization
	void Start () {
		NetworkController = (GameObject)GameObject.Find ("Network Manager");
		man = NetworkController.GetComponent<ImplementedNetworkManager> ();
	}

	//Handle all the button 
	public void OnGUI() {
		//styling
		buttonStyle = new GUIStyle(GUI.skin.GetStyle("box"));
		buttonStyle.fontSize = 50; //change the font size
		buttonStyle.alignment = TextAnchor.MiddleCenter;

		if (GUI.Button (new Rect (0, 3 * h / 10, w / 6, h / 6), "Main Menu", buttonStyle)) {
			//stop the host
			man.StopHost();
			SceneManager.LoadScene ("MainMenu");
		}
	}
}

