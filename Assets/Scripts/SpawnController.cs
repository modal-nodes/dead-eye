﻿using UnityEngine;
using System.Collections;

public class SpawnController {
	
	private Vector3[] SpawnArray;
	private int index;
	private static float RANGE=40f;
	private static float Y_SPAWN = 40f; 

	// Controls spawn locations 
	public SpawnController(){
		SpawnArray = new Vector3[4];

		index = 0;
		SpawnArray [0] = new Vector3 (3, 2, 0);
		SpawnArray [1] = new Vector3 (-3, 2, 0);
		SpawnArray [2] = new Vector3 (5, 2, 1);
		SpawnArray [3] = new Vector3 (-3, 2, -1);
	}

	// Round robin from the added spawn locations
	public Vector3 getNextSpawnLocation(){
		Vector3 nextLocation = SpawnArray [index];
		index = (index + 1) % SpawnArray.Length;
		return nextLocation;
	}

	// Generates a random place to spawn within a defined range on the map. 
	public Vector3 getRandomSpawnLocation() {
		Vector3 randomLocation = new Vector3 (Random.Range(-RANGE, RANGE), Y_SPAWN, Random.Range(-RANGE, RANGE));
		return randomLocation;
	}
}