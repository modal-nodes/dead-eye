﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using System.Collections.Generic;


//Responsible for managing the network connections (over lan or matchmaking online).
//Also used to implement a simple chat server to sending text messages between players.


public class ImplementedNetworkManager : NetworkManager {

	//Statics
	private int screenXDivide = (Screen.width - buttonWidth) / 2;
	int screenWidth = Screen.width;
	static int screenHeight = Screen.height;
	int buttonHeight =  screenHeight / 4;
	// Params for the buttons on the right of the screen
	private static readonly int buttonWidth = Screen.width/5;
	private bool on_chat=false;
	//List of hot chat buttons and sentences. 
	private static readonly List<string> hot_chats = new List<string>(new string[] { 
		"Fire your weapon, damnit!", 
		"Hide behind the wall, son!",
		"2 enemies, 1 potato, and me walk into a bar. I walk out. Alone.",
		"You're doing as well as moonshine on a summer's day"});
	private static readonly List<string> chat_labels = new List<string>(new string[] { 
		"Ofnsve", "Dfnsv","Taunt","Praise"});

	//Class storage
	public List<string> chatHistory = new List<string> ();
	public string currentMessage = string.Empty;
	private GUIStyle buttonStyle, chatButton,labelStyle;



	// hook into NetworkManager client setup process
	public override void OnStartClient(NetworkClient mClient)
	{
		base.OnStartClient(mClient); // base implementation is currently empty

		mClient.RegisterHandler((short)MyMessages.MyMessageTypes.CHAT_MESSAGE, OnClientChatMessage);
	}



	// hook into NetManagers server setup process
	public override void OnStartServer()
	{
		base.OnStartServer(); //base is empty
		NetworkServer.RegisterHandler((short)MyMessages.MyMessageTypes.CHAT_MESSAGE, OnServerChatMessage);
	}

	//Handle server chat message
	private void OnServerChatMessage(NetworkMessage netMsg)
	{
		var msg = netMsg.ReadMessage<StringMessage>();
		Debug.Log("New chat message on server: " + msg.value);
		MyMessages.ChatMessage chat = new MyMessages.ChatMessage();
		chat.message = msg.value;
		NetworkServer.SendToAll((short)MyMessages.MyMessageTypes.CHAT_MESSAGE, chat);
	}


	//Handle client chat message
	private void OnClientChatMessage(NetworkMessage netMsg)
	{
		var msg = netMsg.ReadMessage<StringMessage>();
		Debug.Log("New chat message on client: " + msg.value);
		MyMessages.ChatMessage chat = new MyMessages.ChatMessage();
		chatHistory.Add(msg.value);
	}

	//Run on loop to handle all the chat GUI stuff. 
	private void OnGUI()
	{ 	
		//If we're connected and they've clicked the chat button
		if (isNetworkActive && on_chat) {
			render_chat_box ();
			//routine to render the quick chat stuff
			quick_chat_driver ();
			if (render_quit_chat_button ())
				//So the chat button will disappear
				on_chat = false;
		}

		//If we're connected but they haven't hit the chat button yet, then render the chat button.
		if (isNetworkActive && !on_chat) {
			if (chat_button ())
				on_chat = true;
		}

		//If we're not connected, ensure on_chat is set to false.
		if (!isNetworkActive)
			on_chat = false;
	}


	//Facilitates the sending of the currentMessage through the network manager.
	public void sendMessage() {
		if (!string.IsNullOrEmpty (currentMessage.Trim ())) {
			//storage
			MyMessages.ChatMessage msg = new MyMessages.ChatMessage ();
			//assign the current message
			msg.message = currentMessage;
			//send the message
			ImplementedNetworkManager.singleton.client.Send ((short)MyMessages.MyMessageTypes.CHAT_MESSAGE, msg);
			currentMessage = string.Empty;
		}
	}


	//Renders the chat button on the rhs with the other buttons.
	private bool chat_button() {
		buttonStyle = new GUIStyle(GUI.skin.GetStyle("box"));
		buttonStyle.fontSize = 50; //change the font size
		buttonStyle.alignment = TextAnchor.MiddleCenter;
		return GUI.Button (new Rect (screenWidth - Screen.width/5, screenHeight - 4 * buttonHeight, Screen.width/5, buttonHeight), "Chat", buttonStyle);
	}

	//Renders the Hide Chat button (only called if chat's already open, of course.)
	private bool render_quit_chat_button() {
		return GUI.Button (new Rect (screenWidth - 2* buttonWidth, 0, buttonWidth, buttonHeight / 2), "Hide Chat", buttonStyle);
	}



	//Setups the generation of the quick chat buttons. 
	private void quick_chat_driver() {
		//Draw the quick chat buttons, in white.
		GUI.contentColor = Color.white;
		int index = 0;
		//reanders each button in the list with the matching label. 
		foreach (var quick_chat in hot_chats) {
			render_quick_chat_button (quick_chat,index, chat_labels[index]);
			index++;
		}
		index = 0;
	}



	//Renders the chat box with the send button, and decides whether the messages should be displayed.
	private void render_chat_box() {
		//Styling for the text entry box
		labelStyle = new GUIStyle(GUI.skin.GetStyle("label"));
		labelStyle.fontSize = 40;
		//styling for the send button itself
		chatButton = new GUIStyle(GUI.skin.GetStyle("box"));
		chatButton.fontSize = 40; //change the font size
		chatButton.alignment = TextAnchor.MiddleCenter;

		GUILayout.Space (270);
		GUILayout.BeginHorizontal (GUILayout.Width (250));
		//store the message written in the textfield. 
		currentMessage = GUILayout.TextField (currentMessage);

		//If the send button's pressed, send the message across the network.
		if (GUI.Button (new Rect(screenWidth - Screen.width, screenHeight - 4 * buttonHeight, Screen.width/5, buttonHeight), "Send", chatButton)) {
			sendMessage ();
		}
		GUILayout.EndHorizontal ();

		//Take the last 10 messages sent and display them (if the user has chat enabled.)
		List<string> subchatHistory = new List<string> ();
		if (chatHistory.Count<10) {
			subchatHistory = chatHistory;
		}
		else {
			subchatHistory = chatHistory.GetRange(chatHistory.Count-10,10);
			}
		foreach (var msg in subchatHistory) {
			//Display the messages, in white.
			GUI.contentColor = Color.white;
			GUILayout.Label (msg, labelStyle);
		}
	}


	//renders the quick chat button at the required offset. 
	private void render_quick_chat_button (string quick_chat, int position, string label) {
		//styling
		chatButton = new GUIStyle(GUI.skin.GetStyle("box"));
		chatButton.fontSize = 40; //change the font size
		chatButton.alignment = TextAnchor.MiddleCenter;
		//offsets to assign the grid position of the quick chat button top right.
		int x_offset = 0, y_offset = 0;
		switch (position) {
			case 0:
				x_offset = 2;
				y_offset = 0;
				break;
			case 1:
				x_offset = 1;
				y_offset = 1;
				break;
			case 2:
				x_offset = 1;
				y_offset = 0;
				break;
			case 3:
				x_offset = 2;
				y_offset = 1;
				break;
			default:
				break;
		}

		//send the message if the button is pressed.
		if ( GUI.Button(new Rect(screenWidth - x_offset*buttonWidth/2,y_offset*buttonHeight/2,buttonWidth/2,buttonHeight/2), label, chatButton)) {
			currentMessage = quick_chat;
			sendMessage ();
		}
	}
}

