﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AudioController : MonoBehaviour {

	// Changes the volume of the AudioListener on the player
	public void setVolume(Slider s) {

		AudioListener.volume = s.value;

	}
}
